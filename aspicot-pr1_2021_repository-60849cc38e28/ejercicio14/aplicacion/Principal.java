package aplicacion;

import dominio.ModeloOrdenador;
import dominio.OfertaOrdenador;

public class Principal{
    public static void inicio(){
        ModeloOrdenador portatil = new ModeloOrdenador();
        portatil.setIdentificadorDeModelo("HP");
        portatil.setMemoriaRam("16 GB");
        portatil.setDiscoDuro("256 GB");
        
        OfertaOrdenador oferta = new OfertaOrdenador();
        oferta.setPrecioInicial(1000);
        oferta.setDescuento(100);
        
        oferta.setTieneOrdenadorOfertado(portatil);
        System.out.println(oferta);
    }
}