package dominio;

public class OfertaOrdenador{
    private int precioInicial;
    private int descuento;
    private ModeloOrdenador tieneOfertaOrdenador;
    
    public int getPrecioInicial(){
        return precioInicial;
    }
    
    public void setPrecioInicial(int precioInicial){
        this.precioInicial = precioInicial;
    }
    
    public int getDescuento(){
        return descuento;
    }
    
    public void setDescuento(int descuento){
        this.descuento = descuento;
    }
    
    public ModeloOrdenador getTieneOfertaOrdenador(){
        return tieneOfertaOrdenador;
    }
    
    public void setTieneOrdenadorOfertado(ModeloOrdenador tieneOfertaOrdenador){
        this.tieneOfertaOrdenador = tieneOfertaOrdenador;
    }
    
    public int calcularPrecioFinal(){
        return precioInicial - descuento;
    }
    
    public String toString(){
        String informacionOferta = "El siguiente ordenador está de oferta\n" + tieneOfertaOrdenador;
        informacionOferta += "\nEl precio inicial es " + precioInicial;
        informacionOferta += "\nEl descuento es de " + descuento;
        informacionOferta += "\nEl precio final es de " + calcularPrecioFinal();
        
        return informacionOferta;
    }
}